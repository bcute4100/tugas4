package com.example.kalkulatortugasintent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button b0, b1, b2, b3, b4, b5, b6, b7, b8, b9;
    Button bt, bk, bkl, bb;
    Button bc, bsd;
    TextView operasi;
    float angka1, angka2;
    boolean Tambah, Kurang, Kali, Bagi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b0 = findViewById(R.id.b0);
        b1 = findViewById(R.id.b1);
        b2 = findViewById(R.id.b2);
        b3 = findViewById(R.id.b3);
        b4 = findViewById(R.id.b4);
        b5 = findViewById(R.id.b5);
        b6 = findViewById(R.id.b6);
        b7 = findViewById(R.id.b7);
        b8 = findViewById(R.id.b8);
        b9 = findViewById(R.id.b9);

        bt = findViewById(R.id.bt);
        bk = findViewById(R.id.bk);
        bk = findViewById(R.id.bk);
        bb = findViewById(R.id.bb);

        bsd = findViewById(R.id.bsd);
        bc = findViewById(R.id.bc);
        operasi = findViewById(R.id.operasi);

        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "0"); }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "1");
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "2");
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "9");
            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (operasi == null) {
                    operasi.setText("");
                } else {
                    angka1 = Float.parseFloat(operasi.getText() + "");
                    Tambah = true;
                    operasi.setText(null);
                }
            }
        });
        bk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka1 = Float.parseFloat(operasi.getText() + "");
                Kurang = true;
                operasi.setText(null);
            }
        });
        bb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka1 = Float.parseFloat(operasi.getText() + "");
                Bagi = true;
                operasi.setText(null);
            }
        });
        bkl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka1 = Float.parseFloat(operasi.getText() + "");
                Kali = true;
                operasi.setText(null);
            }
        });

        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText("");
            }
        });
        bsd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka2 = Float.parseFloat(operasi.getText() + "");
                if (Tambah == true) {
                    operasi.setText(angka1 + angka2 + "");
                    Tambah = false;
                }
                if (Kurang == true) {
                    operasi.setText(angka1 - angka2 + "");
                    Kurang = false;
                }
                if (Bagi == true) {
                    operasi.setText(angka1 / angka2 + "");
                    Bagi = false;
                }
                if (Kali == true) {
                    operasi.setText(angka1 * angka2 + "");
                    Kali = false;
                }
                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                String message = operasi.getText().toString();
                i.putExtra("result", message);
                startActivity(i);
            }
        });
    }
}